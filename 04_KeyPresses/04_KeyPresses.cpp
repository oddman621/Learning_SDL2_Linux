#include <SDL2Framework.h>

enum KeyPressSurfaces
{
	KEY_PRESS_SURFACE_DEFAULT = 0,
	KEY_PRESS_SURFACE_UP,
	KEY_PRESS_SURFACE_DOWN,
	KEY_PRESS_SURFACE_LEFT,
	KEY_PRESS_SURFACE_RIGHT,
	KEY_PRESS_SURFACE_HELLO,
};

const char *upSrc = "up.bmp";
const char *downSrc = "down.bmp";
const char *leftSrc = "left.bmp";
const char *rightSrc = "right.bmp";
const char *helloSrc = "image.bmp";
const char *anotherSrc = "another.bmp";
const char *xoutSrc = "xoutwin.bmp";

int main(int argc, char *argv[])
{
	bool quit = false;
	SDL_Event e;

	SDL2Framework fw;
	SDL_Surface *surfaces[] = {
		SDL_LoadBMP(anotherSrc), SDL_LoadBMP(upSrc), SDL_LoadBMP(downSrc),
		SDL_LoadBMP(leftSrc), SDL_LoadBMP(rightSrc), SDL_LoadBMP(helloSrc)
	};
	SDL_Surface *xoutSurface = SDL_LoadBMP(xoutSrc);
	SDL_Surface *currentSurface = surfaces[KEY_PRESS_SURFACE_HELLO];

	int surfaceArraySize = sizeof(surfaces)/sizeof(SDL_Surface*);
	for(int i = 0; i<surfaceArraySize; i++)
	{
		if(surfaces[i] == NULL)
			printf("An image not found!\n");
	}

	while(!quit)
	{
		SDL_PollEvent(&e);
		if(e.type == SDL_QUIT)
			quit = true;
		else if(e.type == SDL_KEYDOWN)
		{
			switch(e.key.keysym.sym)
			{
			case SDLK_UP:
				currentSurface = surfaces[KEY_PRESS_SURFACE_UP];
				break;
			case SDLK_DOWN:
				currentSurface = surfaces[KEY_PRESS_SURFACE_DOWN];
				break;
			case SDLK_LEFT:
				currentSurface = surfaces[KEY_PRESS_SURFACE_LEFT];
				break;
			case SDLK_RIGHT:
				currentSurface = surfaces[KEY_PRESS_SURFACE_RIGHT];
				break;
			default:
				currentSurface = surfaces[KEY_PRESS_SURFACE_DEFAULT];
				break;
			}
		}
		SDL_BlitScaled(currentSurface, NULL, fw.screenSurface, NULL);
		SDL_UpdateWindowSurface(fw.window);
		SDL_Delay(10);
	}

	SDL_BlitScaled(xoutSurface, NULL, fw.screenSurface, NULL);
	SDL_UpdateWindowSurface(fw.window);
	SDL_Delay(2000);

	SDL_FreeSurface(xoutSurface);
	for(int i=0; i<surfaceArraySize;i++)
	{
		SDL_FreeSurface(surfaces[i]);
	}
	return 0;
}
