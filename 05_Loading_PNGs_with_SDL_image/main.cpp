#include "../SDL2Framework/SDL2Framework.h"

int main(int argc, char *argv[])
{
	bool quit = false;
	SDL_Event e;

	SDL2Framework fw;

	SDL2Surface pngSurface("this_is.png");

	while(!quit)
	{
		SDL_PollEvent(&e);
		if(e.type == SDL_QUIT)
			quit = true;

		SDL_BlitSurface(&pngSurface, NULL, fw.screenSurface, NULL);
		SDL_UpdateWindowSurface(fw.window);
	}

	return 0;
}
