#include <SDL2Framework.h>

int main(int argc, char *argv[])
{
	bool quit = false;
	SDL_Event e;

	SDL2Framework fw;
	SDL_Surface *helloWorld = NULL;
	SDL_Surface *xOut = NULL;
	if(argc==2)
		helloWorld=fw.LoadImage(argv[1]);
	else
		helloWorld = fw.LoadImage("image.bmp");
	if(argc==3)
		xOut = fw.LoadImage(argv[2]);
	else
		xOut = fw.LoadImage("xoutwin.bmp");

	if(!helloWorld || !xOut)
		return 0;

	SDL_BlitSurface(helloWorld, NULL, fw.screenSurface, NULL);
	SDL_UpdateWindowSurface(fw.window);

	while(!quit)
	{
		SDL_PollEvent(&e);
		if(e.type == SDL_QUIT)
			quit = true;
	}

	SDL_BlitScaled(xOut, NULL, fw.screenSurface, NULL);
	SDL_UpdateWindowSurface(fw.window);
	SDL_Delay(2000);

	SDL_FreeSurface(xOut);
	SDL_FreeSurface(helloWorld);
	return 0;
}
