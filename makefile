MY_SDL2_FRAMEWORK = SDL2Framework

CC = g++
LIBS = -lSDL2 -l$(MY_SDL2_FRAMEWORK) -lSDL2_image
INCLUDE = -I$(MY_SDL2_FRAMEWORK)
LIBPATH = -L$(MY_SDL2_FRAMEWORK)


#LINK_ORDER = $(CC) $^ $(INCLUDE) $(LIBPATH) $(LIBS) -o $@/$@
LINK_ORDER = $(CC) $(filter %.o, $^) $(INCLUDE) $(LIBPATH) $(LIBS) -o $@/$@
COMPILE_ORDER = $(CC) $(INCLUDE) -c $^ -o $@

%.o : %.cpp
	$(COMPILE_ORDER)

%.o : %.c
	$(COMPILE_ORDER)

$(MY_SDL2_FRAMEWORK) : $(MY_SDL2_FRAMEWORK)/$(MY_SDL2_FRAMEWORK).o
	ar rc $@/lib$(MY_SDL2_FRAMEWORK).a $(filter %.o, $^)

lazyfoo_02=02_GettingAnImageOnTheScreen
$(lazyfoo_02) : $(lazyfoo_02)/sauce.o $(MY_SDL2_FRAMEWORK)
	$(LINK_ORDER)

lazyfoo_03=03_EventDrivenProgramming
$(lazyfoo_03) : $(lazyfoo_03)/$(lazyfoo_03).o $(MY_SDL2_FRAMEWORK)
	$(LINK_ORDER)

lazyfoo_04=04_KeyPresses
$(lazyfoo_04) : $(lazyfoo_04)/$(lazyfoo_04).o $(MY_SDL2_FRAMEWORK)
	$(LINK_ORDER)

lazyfoo_05=05_Loading_PNGs_with_SDL_image
$(lazyfoo_05) : $(lazyfoo_05)/main.o $(MY_SDL2_FRAMEWORK)
	$(LINK_ORDER)
