#include <SDL2Framework.h>

int main(int argc, char *argv[])
{
	SDL2Framework fw;
	SDL_Surface *helloWorld = NULL;
	if(argc==2)
		helloWorld=fw.LoadImage(argv[1]);
	else
		helloWorld = fw.LoadImage("image.bmp");

	if(!helloWorld)
		return 0;

	SDL_BlitSurface(helloWorld, NULL, fw.screenSurface, NULL);
	SDL_UpdateWindowSurface(fw.window);
	SDL_Delay(2000);

	SDL_FreeSurface(helloWorld);
	return 0;
}
