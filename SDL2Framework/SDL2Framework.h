#include <SDL2/SDL.h>
//#include <stdio.h>
//#include <iostream>
#include <string>

//더이상 사용되지 않거나 사용을 권장하지 않는 함수들을 표시하고 경고할 지시문들
#ifdef __GNUC__
#define DEPRECATED(func) func __attribute__ ((deprecated))
#elif defined(__MSC_VER)
#define DEPRECATED(func) __declspec(deprecated) func
#else
#pragma message("WARNING: You need to implement DEPRECATED for this compiler")
#define DEPRECATED(func) func
#endif

class SDL2Framework
{
 protected:
  SDL_Window *mainWindow = NULL;
  SDL_Surface *mainSurface = NULL;
  int mainWidth = 640, mainHeight = 480;
  int initRetVal = 0;
 public:
  SDL_Window *const &window = mainWindow;
  SDL_Surface *const &screenSurface = mainSurface;
  int const &screen_width = mainWidth, &screen_height = mainHeight;
  int const &initVal = initRetVal;
  SDL2Framework();
  SDL2Framework(int width, int height);
  virtual ~SDL2Framework();

		//These are gonna be deprecated. 2017-07-06
public:

		DEPRECATED( static SDL_Surface *LoadImage(const char *src));
		DEPRECATED(static SDL_Surface *LoadImage(const char *src, SDL_PixelFormat *fmt));
};

class SDL2Surface
{
public:
		SDL2Surface();
		SDL2Surface(const char *src);
		virtual ~SDL2Surface();
protected:
		SDL_Surface *_surface=NULL;
public:
		SDL_Surface *const &surface = _surface;
public:

public:
		void LoadMedia(std::string src);
};

SDL_Surface* const& operator&(SDL2Surface &sfc);
