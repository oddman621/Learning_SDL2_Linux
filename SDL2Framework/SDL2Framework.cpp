#include "SDL2Framework.h"
//#include <SDL2/SDL.h>
#include <iostream>
//#include <string>
#include <SDL2/SDL_image.h>
//#include <SDL2/SDL_mixer.h>
//#include <SDL2/SDL_ttf.h>

SDL2Framework::SDL2Framework()
{
  if(SDL_Init(SDL_INIT_VIDEO) < 0)
  {
	printf("SDL could not initialize! SDL_Error: %s\n",
		   SDL_GetError());
	initRetVal = -1;
  }
  else
  {
	  mainWindow = SDL_CreateWindow(
		  "SDL2 Framework Window",
		  SDL_WINDOWPOS_UNDEFINED,
		  SDL_WINDOWPOS_UNDEFINED,
		  mainWidth,
		  mainHeight,
		  SDL_WINDOW_SHOWN);
	  if(window == NULL)
	  {
		  printf("Window could not be created! SDL_Error: %s\n",
				 SDL_GetError());
		  initRetVal = -2;
	  }
	  else
	  {
		  mainSurface = SDL_GetWindowSurface(mainWindow);
			IMG_Init(IMG_INIT_JPG);
			IMG_Init(IMG_INIT_PNG);
	  }
  }
}

SDL2Framework::SDL2Framework(int width, int height)
{
	mainWidth=width; mainHeight=height;
	SDL2Framework();
}

SDL2Framework::~SDL2Framework()
{
	SDL_DestroyWindow(window);
	IMG_Quit();
	SDL_Quit();
}


SDL_Surface *SDL2Framework::LoadImage(const char *src)
{
	SDL_Surface *imageSurface = SDL_LoadBMP(src);
	if(imageSurface == NULL)
		printf("Unable to load image!\n"
			   "src: %s\n"
			   "SDL_Error: %s\n",
			   src, SDL_GetError());
	return imageSurface;
}

SDL_Surface *SDL2Framework::LoadImage(const char *src, SDL_PixelFormat *fmt)
{
	SDL_Surface *loadedSurface = LoadImage(src);
	SDL_Surface *optimizedSurface = SDL_ConvertSurface(loadedSurface, fmt, NULL);
	if(optimizedSurface == NULL)
		printf("Unable to convert image!\n"
			   "src: %s\n"
			   "SDL_Error: %s\n",
			   src, SDL_GetError());
	
	SDL_FreeSurface(loadedSurface);
	return optimizedSurface;
}

SDL2Surface::SDL2Surface(){}
SDL2Surface::SDL2Surface(const char *src)
{
		LoadMedia(src);
}

SDL2Surface::~SDL2Surface()
{
		if(_surface) SDL_FreeSurface(_surface);
		_surface=NULL;
}

void SDL2Surface::LoadMedia(std::string src)
{
		SDL_Surface *loadedSurface=NULL;
 		SDL_Surface *optimizedSurface=NULL;
		std::string extension = src.substr(src.find_last_of(".")+1);

		try
		{
				if(extension == "BMP" || extension == "bmp")
						loadedSurface=SDL_LoadBMP(src.c_str());
				else
						loadedSurface=IMG_Load(src.c_str());
				if(!loadedSurface) throw SDL_GetError();

				optimizedSurface = SDL_ConvertSurface(loadedSurface, loadedSurface->format, NULL);
				SDL_FreeSurface(loadedSurface);
				if(!optimizedSurface) throw SDL_GetError();
				if(_surface) SDL_FreeSurface(_surface);
				_surface=optimizedSurface;
		}
		catch(const char *errmsg)
		{
				std::cout<<"SDL2Surface::LoadMedia(std::string) : Can't create media."<<std::endl
								 <<"source: "<<src<<std::endl
								 <<errmsg<<std::endl;
		}
}

SDL_Surface* const& operator&(SDL2Surface &sfc) 
{
		return sfc.surface;
}
